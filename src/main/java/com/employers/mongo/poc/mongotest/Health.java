package com.employers.mongo.poc.mongotest;

public class Health {
    public Health(String version) {
        this.version = version;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    String version;
}
