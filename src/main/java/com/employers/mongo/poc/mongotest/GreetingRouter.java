package com.employers.mongo.poc.mongotest;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.RequestPredicates.accept;

@Configuration
public class GreetingRouter {
    @Bean
    public RouterFunction<ServerResponse> route(GreetingHandler greetingHandler) {

        return RouterFunctions
                .route(GET("/hello").and(accept(MediaType.TEXT_PLAIN)), greetingHandler::hello)
                .andRoute(GET("/version").and(accept(APPLICATION_JSON)), greetingHandler::version)
                .andRoute(GET("/collectionnames").and(accept(APPLICATION_JSON)), greetingHandler::collectionNames);
    }
}
