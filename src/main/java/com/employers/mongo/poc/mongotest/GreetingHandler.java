package com.employers.mongo.poc.mongotest;

import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.http.MediaType.TEXT_PLAIN;

@Component
public class GreetingHandler {
    Logger LOG= LoggerFactory.getLogger(GreetingHandler.class);
    private ReactiveMongoTemplate reactiveMongoTemplate;

    public GreetingHandler(ReactiveMongoTemplate reactiveMongoTemplate){
        this.reactiveMongoTemplate=reactiveMongoTemplate;
    }
    public Mono<ServerResponse> hello(ServerRequest request) {
        LOG.info("reactive template {}", reactiveMongoTemplate.getMongoDatabase().getName());
        return ServerResponse.ok().contentType(TEXT_PLAIN)
                .body(BodyInserters.fromObject("Hello, Mongo Test!"));
    }

    public Mono<ServerResponse> version(ServerRequest request) {
        Mono<Document> documentMono =reactiveMongoTemplate.executeCommand("{ buildInfo: 1 }");
        return 	ServerResponse.ok().contentType(APPLICATION_JSON).body(documentMono.map((document) -> up(document)), Health.class);


    }

    public Mono<ServerResponse> collectionNames(ServerRequest request) {
        Flux<String> collectionNames = reactiveMongoTemplate.getCollectionNames();
        return 	ServerResponse.ok().contentType(APPLICATION_JSON).body(collectionNames, String.class);


    }

    private Health up( Document document) {
        return new Health("version" + document.getString("version"));
    }
}
