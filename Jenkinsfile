#!/usr/bin/groovy
node('') {

    properties([
            parameters([
                    choice(choices: 'Dev', defaultValue: "Dev", description: 'Choose Environment', name: 'Environment'),
                    choice(choices: 'Build', defaultValue: "Build", description: 'Choose Build Mode', name: 'Mode')

            ]),
            [$class: 'BuildConfigProjectProperty', name: '', namespace: '', resourceVersion: '', uid: ''],
            buildDiscarder(logRotator(artifactDaysToKeepStr: '', artifactNumToKeepStr: '', daysToKeepStr: '1', numToKeepStr: '3')),
            pipelineTriggers([cron('H 7 * * 1-5/1')]) //Accommodates GMT, i.e. 7:00 AM GMT == 2:00 AM Central Time
    ])
    env.NAMESPACE = "employers-ci-cd"
    env.DEV_PROJECT = "endorsements-dev"
    env.SOURCE_CONTEXT_DIR = ""
    env.UBER_JAR_CONTEXT_DIR = "target/"
    env.MVN_COMMAND = "clean deploy "
    env.MVN_NO_TEST_COMMAND = "clean deploy  -DskipTests"
    env.MVN_SNAPSHOT_DEPLOYMENT_REPOSITORY = "nexus::default::http://nexus:8081/repository/maven-snapshots"
    env.MVN_RELEASE_DEPLOYMENT_REPOSITORY = "nexus::default::http://nexus:8081/repository/maven-releases"
    env.APP_NAME = "${env.JOB_NAME}".replaceAll(/-?${env.PROJECT_NAME}-?/, '').replaceAll(/-?pipeline-?/, '')
    env.OCP_API_SERVER = "${env.OPENSHIFT_API_URL}"
    env.OCP_TOKEN = readFile('/var/run/secrets/kubernetes.io/serviceaccount/token').trim()
    env.REPLICA_COUNT = 1
    env.ENVIRONMENT = "${params.Environment}"
    env.MODE = "${params.Mode}"
    env.VERSION = "${params.Version}"
    env.POM_VERSION = "0.0.1"
    env.BRANCH = "origin/master"
}


/**
 this section of the pipeline executes on a custom mvn build slave.
 you should not need to change anything below unless you need new stages or new integrations (e.g. Cucumber Reports or Sonar)
 **/
mailTo = 'nsriram@employers.com,lwatson@employers.com'

node('mvn-build-pod') {
    try {



        stage('SCM Checkout') {
            checkout([$class                           : 'GitSCM', branches: [[name: env.BRANCH]],
                      doGenerateSubmoduleConfigurations: false,
                      extensions                       : [[$class: 'CleanBeforeCheckout'], [$class: 'LocalBranch']]
                      , submoduleCfg                   : [],
                      userRemoteConfigs                : scm.userRemoteConfigs])

            GIT_COMMIT_MSG = sh(script: "git log --name-status HEAD^..HEAD", returnStdout: true)
        }
        stage('Get POM version') {

            pom = readMavenPom file: 'pom.xml'
            env.POM_VERSION = pom.version.replaceAll('-SNAPSHOT', '')
            echo "POM Version " + env.POM_VERSION

        }

        dir("${env.SOURCE_CONTEXT_DIR}") {
            stage('Build App') {
                // verify nexus is up or the build will fail with a strange error
                openshiftVerifyDeployment(
                        apiURL: "${env.OCP_API_SERVER}",
                        authToken: "${env.OCP_TOKEN}",
                        depCfg: 'nexus',
                        namespace: "${env.NAMESPACE}",
                        verifyReplicaCount: true,
                        waitTime: '3',
                        waitUnit: 'min'
                )
                sh "mvn ${env.MVN_COMMAND} -DaltDeploymentRepository=${MVN_SNAPSHOT_DEPLOYMENT_REPOSITORY}"

            }




            stage('Build Image') {
                sh "oc start-build ${env.APP_NAME} --from-dir=${env.UBER_JAR_CONTEXT_DIR} -w"

            }
        }





        stage('Deploy to Dev') {
            openshiftTag(apiURL: "${env.OCP_API_SERVER}", authToken: "${env.OCP_TOKEN}", destStream: "${env.APP_NAME}", destTag: "${env.POM_VERSION}", destinationAuthToken: "${env.OCP_TOKEN}", destinationNamespace: "${env.DEV_PROJECT}", namespace: "${env.NAMESPACE}", srcStream: "${env.APP_NAME}", srcTag: 'latest')

        }
    } catch (e) {
        currentBuild.result = "FAILED"
        throw e
    }
}

